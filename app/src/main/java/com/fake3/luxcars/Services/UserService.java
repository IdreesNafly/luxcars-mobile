package com.fake3.luxcars.Services;

import com.fake3.luxcars.Models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    String BASE_URL = "http://192.168.43.126:8080/api/auth/";

    @POST("signup")
    Call<User> registerUser(@Body User user);
}
