package com.fake3.luxcars.Services;

import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.Models.Vehicle;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface VehicleService {

    String BASE_URL = "http://192.168.43.126:8080/api/vehicle/";

    @GET("getAllVehicles")
    Call<List<Vehicle>> getVehicles();

}
