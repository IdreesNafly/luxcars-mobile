package com.fake3.luxcars.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fake3.luxcars.Fragments.DetailFragment;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;

import java.util.List;

import javax.crypto.Cipher;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder> {

    private Context context;
    private List<Vehicle> vehicleList;


    public VehicleAdapter(Context context, List<Vehicle> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
    }

    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.car_adapter,null);
        VehicleViewHolder holder=new VehicleViewHolder(view);
        return  holder;
}

    @Override
    public void onBindViewHolder(@NonNull VehicleViewHolder holder, int position) {
        Vehicle vehicle = vehicleList.get(position);
        holder.imageView.setImageResource(R.drawable.ic_person_black_24dp);
        holder.name.setText(vehicle.getVehicleType());
        holder.price.setText(String.valueOf(vehicle.getVehiclePrice()));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.layout,new DetailFragment()).commit();
            }
        });
    }



    @Override
    public int getItemCount() {
         return vehicleList.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView name,price;
        CardView cardView;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.cardView);
            imageView =itemView.findViewById(R.id.imageView);
            name=itemView.findViewById(R.id.textViewShortDesc);
            price=itemView.findViewById(R.id.textViewPrice);


        }
    }

}
