package com.fake3.luxcars.Models;

public class User {

    private String Email;
    private  String FullName;
    private  int Number;
    private String Password;

    public User() {
    }

    public User(String email, String fullName, int number, String password) {
        Email = email;
        FullName = fullName;
        Number = number;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
