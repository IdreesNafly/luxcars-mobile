package com.fake3.luxcars.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.fake3.luxcars.Fragments.Search;
import com.fake3.luxcars.R;

import java.util.ArrayList;
import java.util.List;

public class DetailProduct extends AppCompatActivity {

    Button share, call,makeAppointment,reserve,more,saveList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);
        makeAppointment=findViewById(R.id.appointment);
        reserve=findViewById(R.id.reserve);
        more=findViewById(R.id.more);
        saveList=findViewById(R.id.saveList);
        share = findViewById(R.id.share);
        call = findViewById(R.id.call);




        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+Uri.encode("0776307850")));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
        }
    });


        List<SlideModel> slideModels=new ArrayList<>();
        slideModels.add(new SlideModel("https://picsum.photos/id/896/300/200","Image 1"));
        slideModels.add(new SlideModel("https://picsum.photos/id/894/300/200","Image 2"));
        slideModels.add(new SlideModel("https://picsum.photos/id/892/300/200","Image 3"));
        slideModels.add(new SlideModel("https://picsum.photos/id/891/300/200","Image 4"));



        ImageSlider imageSlider =findViewById(R.id.imageView7);
        imageSlider.setImageList(slideModels,true);
        imageSlider.startSliding(3000) ;// with new period
        imageSlider.stopSliding();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        getSupportFragmentManager().beginTransaction().replace(R.id.layout,new Search()).commit();
    }
}