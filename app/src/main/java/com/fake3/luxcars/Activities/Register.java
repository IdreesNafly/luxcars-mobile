package com.fake3.luxcars.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
Button Register;
TextView Login;
EditText FullName,Email,Number,Password,ConfirmPassword;
String fullName,email,password,confirmPassword;
int number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FullName=findViewById(R.id.name);
        Email=findViewById(R.id.email);
        Number=findViewById(R.id.number);
        Password=findViewById(R.id.password);
        ConfirmPassword=findViewById(R.id.confirmPassword);
        Register=findViewById(R.id.register);
        Login=findViewById(R.id.login);
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initialize();
                User user=new User(email,fullName,number,password);
                retrofit2.Retrofit retrofit=RetrofitService.getRetrofit(UserService.BASE_URL);
                UserService userService=retrofit.create(UserService.class);

                Call<User>pp=userService.registerUser(user);
                pp.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call <User> call, Response<User> response) {
                        User  r=response.body();
                        if(r!=null){
                            Toast.makeText(getApplicationContext(), "User successfully created", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable thtow) {
                    }
                });

            }
        });




    }


    public void initialize(){
        fullName=FullName.getText().toString();
        email=Email.getText().toString();
        password=Password.getText().toString();
        confirmPassword=ConfirmPassword.getText().toString();
        number=Integer.parseInt(Number.getText().toString());


    }
}
