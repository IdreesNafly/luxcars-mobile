package com.fake3.luxcars.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.fake3.luxcars.R;

import java.util.ArrayList;

public class ModelSearch extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_model_search, container, false);

        Spinner dropdown =view.findViewById(R.id.spinner);

        String[] items = new String[]{"All Makes", "2", "three"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);

        dropdown.setAdapter(adapter);


        Spinner dropdown1 =view.findViewById(R.id.spinner2);

        String[] items1 = new String[]{"All Models", "2", "three"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, items1);

        dropdown1.setAdapter(adapter1);

        Spinner dropdown2 =view.findViewById(R.id.spinner3);

        String[] items2 = new String[]{"Year", "2", "three"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, items2);

        dropdown2.setAdapter(adapter2);

        return view;
    }
}