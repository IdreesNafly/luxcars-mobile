package com.fake3.luxcars.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.fake3.luxcars.Activities.DetailProduct;
import com.fake3.luxcars.Adapter.VehicleAdapter;
import com.fake3.luxcars.Models.User;
import com.fake3.luxcars.Models.Vehicle;
import com.fake3.luxcars.R;
import com.fake3.luxcars.Services.RetrofitService;
import com.fake3.luxcars.Services.UserService;
import com.fake3.luxcars.Services.VehicleService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Home extends Fragment {

Button search,value;
    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_home, container, false);

        search=view.findViewById(R.id.search);
        value=view.findViewById(R.id.value);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getContext(), DetailProduct.class);
                startActivity(intent);
             //   getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout,new DetailFragment()).commit();
            }
        });







    return  view;
    }

}
