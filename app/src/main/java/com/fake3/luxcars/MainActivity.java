package com.fake3.luxcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.fake3.luxcars.Fragments.Search;
import com.fake3.luxcars.Fragments.Home;
import com.fake3.luxcars.Fragments.Profile;
import com.fake3.luxcars.Fragments.SavedList;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView=findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListner);


        getSupportFragmentManager().beginTransaction().replace(R.id.layout,new Home()).commit();


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListner= new
            BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment slectedFragment=new Home();
                    switch (menuItem.getItemId()){

                        case R.id.nav_home: slectedFragment=new Home();
                            break;
                        case R.id.nav_search: slectedFragment=new Search();
                            break;
                        case R.id.nav_profile: slectedFragment=new Profile();
                            break;
                        case R.id.nav_settings: slectedFragment=new SavedList();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.layout,slectedFragment).commit();
                    return true;
                }
            };
}
